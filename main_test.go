package main

import "testing"

func TestHello(t *testing.T) {
	var v string
	v = Hello("WORLD")
	if v != "HeLlO WORLD" {
		t.Error("Expected HeLlO WORLD, got ", v)
	}
}
